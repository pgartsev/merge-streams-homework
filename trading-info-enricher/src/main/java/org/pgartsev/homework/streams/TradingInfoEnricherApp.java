package org.pgartsev.homework.streams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradingInfoEnricherApp {

    public static void main(String[] args) {
        SpringApplication.run(TradingInfoEnricherApp.class, args);
    }
}
