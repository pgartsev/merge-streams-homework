package org.pgartsev.homework.streams;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.WindowStore;
import org.pgartsev.homework.streams.model.Tick;
import org.pgartsev.homework.streams.model.Trade;
import org.pgartsev.homework.streams.model.TradingInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.EnableKafkaStreams;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.support.serializer.JsonSerde;

import java.time.Duration;
import java.time.format.DateTimeFormatter;

@Configuration
@EnableKafka
@EnableKafkaStreams
@RequiredArgsConstructor
public class KafkaConf {

    private final TradingInfoRepository tradingInfoRepository;


    @Bean
    public NewTopic topicTick(@Value("${topic.tick}") String topicTick) {
        return TopicBuilder.name(topicTick)
                .partitions(1)
                .build();
    }

    @Bean
    public NewTopic topicTrade(@Value("${topic.trade}") String topicTrade) {
        return TopicBuilder.name(topicTrade)
                .partitions(1)
                .build();
    }

    @Bean
    public NewTopic topicInfo(@Value("${topic.trading.info}") String topicInfo) {
        return TopicBuilder.name(topicInfo)
                .partitions(1)
                .build();
    }

    @Bean
    public KStream<String, TradingInfo> findInfoStream(StreamsBuilder kStreamBuilder,
                                                       @Value("${topic.tick}") String topicTick,
                                                       @Value("${topic.trade}") String topicTrade,
                                                       @Value("${topic.trading.info}") String topicInfo
    ) {

        Consumed<String, Tick> tickConsumed = Consumed.with(Serdes.String(), new JsonSerde<>(Tick.class));
        KStream<String, Tick> ticksStream = kStreamBuilder.stream(topicTick, tickConsumed)
                .selectKey((k, v) -> v.getSymbol())
                .repartition(Repartitioned.with(Serdes.String(), new JsonSerde<>(Tick.class)));

        Consumed<String, Trade> tradeConsumed = Consumed.with(Serdes.String(), new JsonSerde<>(Trade.class));
        KStream<String, Trade> tradesStream = kStreamBuilder.stream(topicTrade, tradeConsumed)
                .selectKey((k, v) -> v.getSymbol())
                .repartition(Repartitioned.with(Serdes.String(), new JsonSerde<>(Trade.class)));

        StreamJoined<String, Trade, Tick> streamJoined = StreamJoined.with(
                Serdes.String(),
                new JsonSerde<>(Trade.class),
                new JsonSerde<>(Tick.class)
        );
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS");
        Grouped<String, TradeAndTick> grouped = Grouped.with(Serdes.String(), new JsonSerde<>(TradeAndTick.class));
        Materialized<String, TradeAndTick, KeyValueStore<Bytes, byte[]>> materialized = Materialized
                .<String, TradeAndTick,  KeyValueStore<Bytes, byte[]>> as("max-trade-and-tick-store")
                .withValueSerde(new JsonSerde<>(TradeAndTick.class))
                .withKeySerde(Serdes.String());
        KStream<String, TradingInfo> infoStream = tradesStream
                .join(ticksStream, TradeAndTick::new, JoinWindows.of(Duration.ofMinutes(5)), streamJoined)
                .filter((k, v) -> v.TickTimeSameOrBeforeTrade())
                .groupBy((k, v) -> k + v.getTrade().getTime().format(formatter), grouped)
//                .windowedBy(TimeWindows.of(Duration.ofMinutes(5)).advanceBy(Duration.ofMinutes(1)))
                .aggregate(TradeAndTick::empty, this::findMaxTickTime, materialized)
                .toStream()
                .mapValues((k,v) -> v.toTradingInfo());
        infoStream.to(topicInfo, Produced.with(Serdes.String(), new JsonSerde<>(TradingInfo.class)));
        return infoStream;
    }

    private TradeAndTick findMaxTickTime(String key, TradeAndTick value, TradeAndTick agg) {
        return agg.isEmpty() || value.getTick().getTime().isAfter(agg.getTick().getTime())
                ? value
                : agg;
    }


    @Bean
    public KStream<String, TradingInfo> infoSaveStream(StreamsBuilder kStreamBuilder,
                                                       @Value("${topic.trading.info}") String infoTopic) {
        KStream<String, TradingInfo> stream = kStreamBuilder
                .stream(infoTopic, Consumed.with(Serdes.String(), new JsonSerde<>(TradingInfo.class)));
        stream.foreach((k, v) -> tradingInfoRepository.save(v));
        return stream;
    }


    @Data
    private static class TradeAndTick {
        private final Trade trade;
        private final Tick tick;

        public boolean TickTimeSameOrBeforeTrade() {
            return tick.getTime().isEqual(trade.getTime())
                    || tick.getTime().isBefore(trade.getTime());
        }

        public TradingInfo toTradingInfo() {
            return new TradingInfo(
                    trade.getId(),
                    trade.getTime(),
                    trade.getSymbol(),
                    tick.getPrice()
            );
        }

        public boolean isEmpty() {
            return trade == null && tick == null;
        }

        public static TradeAndTick empty() {
            return new TradeAndTick(null, null);
        }
    }
}
