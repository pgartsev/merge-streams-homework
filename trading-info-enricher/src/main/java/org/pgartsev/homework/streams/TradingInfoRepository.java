package org.pgartsev.homework.streams;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.pgartsev.homework.streams.model.TradingInfo;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.ZoneOffset;

@Repository
@RequiredArgsConstructor
@Slf4j
public class TradingInfoRepository {

    private final NamedParameterJdbcTemplate namedTemplate;

    public void save(TradingInfo info) {
        log.info("save info {} to db", info);
        final String query = "insert into trades(trade_id, trade_time, trade_symbol, price) " +
                "VALUES (:trade_id, :trade_time, :trade_symbol, :price)";
                final SqlParameterSource params = new MapSqlParameterSource()
                .addValue("trade_id", info.getId())
                .addValue("trade_time", info.getTime().toEpochSecond(ZoneOffset.UTC))
                .addValue("trade_symbol", info.getSymbol())
                .addValue("price", info.getPrice());
        namedTemplate.update(query, params);
    }
}
