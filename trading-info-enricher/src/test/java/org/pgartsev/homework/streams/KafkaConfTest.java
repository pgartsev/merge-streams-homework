package org.pgartsev.homework.streams;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.Produced;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.pgartsev.homework.streams.model.TradingInfo;
import org.springframework.kafka.support.serializer.JsonSerde;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Properties;

class KafkaConfTest {
    private static final String TICK_TOPIC = "ticks";
    private static final String TRADE_TOPIC = "trades";
    private static final String INFO_TOPIC = "infos";


    @Test
    public void testStream() throws InterruptedException {

        TradingInfoRepository repository = Mockito.mock(TradingInfoRepository.class);
        KafkaConf kafkaConf = new KafkaConf(repository);
        StreamsBuilder builder = new StreamsBuilder();
        //send stream to test output topic
        Produced<String, TradingInfo> produced = Produced.with(Serdes.String(), new JsonSerde<>(TradingInfo.class));
        kafkaConf.findInfoStream(builder, TICK_TOPIC, TRADE_TOPIC, INFO_TOPIC);

        TopologyTestDriver testDriver = new TopologyTestDriver(builder.build(), buildStreamConfig());

        TestInputTopic<String, String> tickInput = testDriver
                .createInputTopic(TICK_TOPIC, Serdes.String().serializer(), Serdes.String().serializer());
        TestInputTopic<String, String> tradeInput = testDriver
                .createInputTopic(TRADE_TOPIC, Serdes.String().serializer(), Serdes.String().serializer());
        TestOutputTopic<String, String> infoOut = testDriver.createOutputTopic(
                INFO_TOPIC,
                Serdes.String().deserializer(),
                Serdes.String().deserializer()
        );
        String tick1 = "{\"id\":1,\"time\":\"2021-03-07 07:37:34.969219\",\"symbol\":\"A\",\"price\":1.01517}";
        String tick2 = "{\"id\":2,\"time\":\"2021-03-07 07:37:35.121212\",\"symbol\":\"B\",\"price\":2.91502}";
        String tick3 = "{\"id\":3,\"time\":\"2021-03-07 07:37:35.121210\",\"symbol\":\"B\",\"price\":5.11111}";
        String tick4 = "\"3\":{\"id\":3,\"time\":\"2021-03-07 07:37:35.121209\",\"symbol\":\"B\",\"price\":8.11111}";

        String trade1 = "{\"id\":1,\"time\":\"2021-03-07 07:37:34.969219\",\"symbol\":\"A\"}";
        String trade2 = "{\"id\":2,\"time\":\"2021-03-07 07:37:35.121211\",\"symbol\":\"B\"}";

        tickInput.pipeInput(tick1);
        tickInput.pipeInput(tick2);
        tickInput.pipeInput(tick3);

        tradeInput.pipeInput(trade1);
        tradeInput.pipeInput(trade2);

        Thread.sleep(2000);

        String expectedInfo1 = "{\"id\":1,\"time\":\"2021-03-07 07:37:34.969219\",\"symbol\":\"A\",\"price\":1.01517}";
        String expectedInfo2 = "{\"id\":2,\"time\":\"2021-03-07 07:37:35.121211\",\"symbol\":\"B\",\"price\":5.11111}";
        String info1 = infoOut.readValue();
        assertEquals(expectedInfo1, info1);
        String info2 = infoOut.readValue();
        assertEquals(expectedInfo2, info2);
    }

    private Properties buildStreamConfig() {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "trading-info-enricher-test");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9091");
        return props;
    }
}