# Тестовое задание по kafka streams

Для тестового окружения в docker-compose выбран докер-образ от bitnami, а не от confluent.
Это сделано в связи с тем, что образ от confluent - это не обычная кафка, а их коммерческая сборка с отключенными
коммерческими фичами.

В задании указан некий репозиторий, от которого я должен сделать форк, но в самом письме никаких ссылок на репозитории
не было. 
Ввиду этого, сделал сам так, как смог.

## Упаковать java проложения в докер образы 
`./gradlew dockerBuildImage`

## Запустить всё окружение в docker-compose 
`docker-compose up -d`

## Отправить тестовые сообщения в кафку

`kafkacat -b localhost:9093 -t ticks -T -P -K: -l test-message-ticks.txt`

`kafkacat -b localhost:9093 -t trades -T -P -K: -l test-message-trades.txt`

Посмотреть результат можно в postgres или финальном топике:
1. Топик
`kafkacat -b localhost:9093 -t infos -C -K\\t`
   
2. Postgres доступен по адресу localhost:50010, username - postgres, password - example

