CREATE TABLE trades
(
    trade_id     integer,
    trade_time   integer,
    trade_symbol text,
    price        double precision
);